﻿using CoreCentral;
using Fabrikam;
using Fabrikas;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tartampion;

namespace MaCentrale
{
    public partial class frmCentrale : Form
    {
        private Central theOne;
        private ArrayList thePumps;
        private Timer timer;
        private Timer timerCheck;
        #region Ceci est mon constructeur
        public frmCentrale()
        {
            InitializeComponent();
            theOne = new Central();
            thePumps = new ArrayList();
            timer = new Timer() { Enabled = false, Interval = 800 };
            timer.Tick += Timer_Tick;
            timerCheck = new Timer() { Enabled = true, Interval = 100 };
            timer.Tick += TimerCheck_Tick;
        }

        private void TimerCheck_Tick(object sender, EventArgs e)
        {
            lblTemperature.Text = string.Format("{0} °C", theOne.Temperature);
        }
        #endregion

        #region Evenements de ma form
        private void Timer_Tick(object sender, EventArgs e)
        {
            theOne.Run();
        }

        private void btnRunStop_Click(object sender, EventArgs e)
        {
            timer.Enabled = !timer.Enabled;
            btnRunStop.Text = timer.Enabled ? "Arrêter la centrale" : "Démarrer la centrale";
            if (!timer.Enabled)
                foreach(var p in thePumps)
                {
                    if (p is SuperPompeFabrikam && ((SuperPompeFabrikam)p).EstEnMarche)
                        ((SuperPompeFabrikam)p).Eteindre();
                    if (p is ThePump && ((ThePump)p).IsOn)
                        ((ThePump)p).TurnOff();
                    if (p is TheWeakestPump && ((TheWeakestPump)p).IsOn)
                        ((TheWeakestPump)p).TurnOnOff();
                }
        }

        private void btnAddFabrikam_Click(object sender, EventArgs e)
        {
            var pump = new SuperPompeFabrikam(theOne.Refresh);
            theOne.AddPump(pump.MettreEnMarche, pump.Eteindre);
            thePumps.Add(pump);
        }

        private void btnAddFabrikas_Click(object sender, EventArgs e)
        {
            var pump = new ThePump(theOne.Refresh);
            theOne.AddPump(pump.TurnOn, pump.TurnOff);
            thePumps.Add(pump);
        }

        private void btnTartampion_Click(object sender, EventArgs e)
        {
            var pump = new TheWeakestPump(theOne.Refresh);
            theOne.AddPump(pump.TurnOnOff, pump.TurnOnOff);
            thePumps.Add(pump);
        }

        private void btnRemoveFabrikam_Click(object sender, EventArgs e)
        {
            SuperPompeFabrikam pump = (SuperPompeFabrikam)thePumps.ToArray().FirstOrDefault(p => p is SuperPompeFabrikam);
            if (pump != null)
            {
                theOne.RemovePump(pump.MettreEnMarche, pump.Eteindre);
                thePumps.Remove(pump);
            }
        }

        private void btnRemoveFabrikas_Click(object sender, EventArgs e)
        {
            ThePump pump = (ThePump)thePumps.ToArray().FirstOrDefault(p => p is ThePump);
            if (pump != null)
            {
                theOne.RemovePump(pump.TurnOn, pump.TurnOff);
                thePumps.Remove(pump);
            }
        }

        private void btnRemoveTartampion_Click(object sender, EventArgs e)
        {
            TheWeakestPump pump = (TheWeakestPump)thePumps.ToArray().FirstOrDefault(p => p is TheWeakestPump);
            if (pump != null)
            {
                theOne.RemovePump(pump.TurnOnOff, pump.TurnOnOff);
                thePumps.Remove(pump);
            }
        }
        #endregion
    }
}
