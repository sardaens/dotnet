﻿namespace MaCentrale
{
    partial class frmCentrale
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRunStop = new System.Windows.Forms.Button();
            this.lblTemperature = new System.Windows.Forms.Label();
            this.btnAddFabrikam = new System.Windows.Forms.Button();
            this.btnAddFabrikas = new System.Windows.Forms.Button();
            this.btnTartampion = new System.Windows.Forms.Button();
            this.btnRemoveTartampion = new System.Windows.Forms.Button();
            this.btnRemoveFabrikas = new System.Windows.Forms.Button();
            this.btnRemoveFabrikam = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnRunStop
            // 
            this.btnRunStop.Location = new System.Drawing.Point(13, 13);
            this.btnRunStop.Name = "btnRunStop";
            this.btnRunStop.Size = new System.Drawing.Size(240, 23);
            this.btnRunStop.TabIndex = 0;
            this.btnRunStop.Text = "Démarrer la centrale";
            this.btnRunStop.UseVisualStyleBackColor = true;
            this.btnRunStop.Click += new System.EventHandler(this.btnRunStop_Click);
            // 
            // lblTemperature
            // 
            this.lblTemperature.Location = new System.Drawing.Point(13, 43);
            this.lblTemperature.Name = "lblTemperature";
            this.lblTemperature.Size = new System.Drawing.Size(240, 23);
            this.lblTemperature.TabIndex = 1;
            this.lblTemperature.Text = "20 °C";
            this.lblTemperature.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnAddFabrikam
            // 
            this.btnAddFabrikam.Location = new System.Drawing.Point(16, 69);
            this.btnAddFabrikam.Name = "btnAddFabrikam";
            this.btnAddFabrikam.Size = new System.Drawing.Size(75, 23);
            this.btnAddFabrikam.TabIndex = 2;
            this.btnAddFabrikam.Text = "Fabrikam";
            this.btnAddFabrikam.UseVisualStyleBackColor = true;
            this.btnAddFabrikam.Click += new System.EventHandler(this.btnAddFabrikam_Click);
            // 
            // btnAddFabrikas
            // 
            this.btnAddFabrikas.Location = new System.Drawing.Point(97, 69);
            this.btnAddFabrikas.Name = "btnAddFabrikas";
            this.btnAddFabrikas.Size = new System.Drawing.Size(75, 23);
            this.btnAddFabrikas.TabIndex = 3;
            this.btnAddFabrikas.Text = "Fabrikas";
            this.btnAddFabrikas.UseVisualStyleBackColor = true;
            this.btnAddFabrikas.Click += new System.EventHandler(this.btnAddFabrikas_Click);
            // 
            // btnTartampion
            // 
            this.btnTartampion.Location = new System.Drawing.Point(178, 69);
            this.btnTartampion.Name = "btnTartampion";
            this.btnTartampion.Size = new System.Drawing.Size(75, 23);
            this.btnTartampion.TabIndex = 4;
            this.btnTartampion.Text = "Tartampion";
            this.btnTartampion.UseVisualStyleBackColor = true;
            this.btnTartampion.Click += new System.EventHandler(this.btnTartampion_Click);
            // 
            // btnRemoveTartampion
            // 
            this.btnRemoveTartampion.Location = new System.Drawing.Point(178, 98);
            this.btnRemoveTartampion.Name = "btnRemoveTartampion";
            this.btnRemoveTartampion.Size = new System.Drawing.Size(75, 23);
            this.btnRemoveTartampion.TabIndex = 7;
            this.btnRemoveTartampion.Text = "-Tartampion";
            this.btnRemoveTartampion.UseVisualStyleBackColor = true;
            this.btnRemoveTartampion.Click += new System.EventHandler(this.btnRemoveTartampion_Click);
            // 
            // btnRemoveFabrikas
            // 
            this.btnRemoveFabrikas.Location = new System.Drawing.Point(97, 98);
            this.btnRemoveFabrikas.Name = "btnRemoveFabrikas";
            this.btnRemoveFabrikas.Size = new System.Drawing.Size(75, 23);
            this.btnRemoveFabrikas.TabIndex = 6;
            this.btnRemoveFabrikas.Text = "-Fabrikas";
            this.btnRemoveFabrikas.UseVisualStyleBackColor = true;
            this.btnRemoveFabrikas.Click += new System.EventHandler(this.btnRemoveFabrikas_Click);
            // 
            // btnRemoveFabrikam
            // 
            this.btnRemoveFabrikam.Location = new System.Drawing.Point(16, 98);
            this.btnRemoveFabrikam.Name = "btnRemoveFabrikam";
            this.btnRemoveFabrikam.Size = new System.Drawing.Size(75, 23);
            this.btnRemoveFabrikam.TabIndex = 5;
            this.btnRemoveFabrikam.Text = "-Fabrikam";
            this.btnRemoveFabrikam.UseVisualStyleBackColor = true;
            this.btnRemoveFabrikam.Click += new System.EventHandler(this.btnRemoveFabrikam_Click);
            // 
            // frmCentrale
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(264, 134);
            this.Controls.Add(this.btnRemoveTartampion);
            this.Controls.Add(this.btnRemoveFabrikas);
            this.Controls.Add(this.btnRemoveFabrikam);
            this.Controls.Add(this.btnTartampion);
            this.Controls.Add(this.btnAddFabrikas);
            this.Controls.Add(this.btnAddFabrikam);
            this.Controls.Add(this.lblTemperature);
            this.Controls.Add(this.btnRunStop);
            this.Name = "frmCentrale";
            this.Text = "Ma Centrale";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnRunStop;
        private System.Windows.Forms.Label lblTemperature;
        private System.Windows.Forms.Button btnAddFabrikam;
        private System.Windows.Forms.Button btnAddFabrikas;
        private System.Windows.Forms.Button btnTartampion;
        private System.Windows.Forms.Button btnRemoveTartampion;
        private System.Windows.Forms.Button btnRemoveFabrikas;
        private System.Windows.Forms.Button btnRemoveFabrikam;
    }
}

