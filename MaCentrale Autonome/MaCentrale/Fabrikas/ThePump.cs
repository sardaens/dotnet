﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Fabrikas
{
    public class ThePump
    {
        public delegate void Refresh(int temp);

        private Refresh refresh;
        private Timer timer;
        public bool IsOn => timer.Enabled;
        public ThePump(Refresh funcRefresh)
        {
            timer = new Timer() { Enabled = false, Interval = 900 };
            timer.Elapsed += Timer_Elapsed;
            refresh = funcRefresh;
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            refresh(5);
        }

        public void TurnOn()
        {
            timer.Enabled = true;
        }
        public void TurnOff()
        {
            timer.Enabled = false;
        }
    }
}
