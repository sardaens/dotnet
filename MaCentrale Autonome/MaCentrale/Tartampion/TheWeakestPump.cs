﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Tartampion
{
    public class TheWeakestPump
    {
        public delegate void Refresh(int temp);

        private Refresh refresh;
        private Timer timer;
        public bool IsOn => timer.Enabled;
        public TheWeakestPump(Refresh funcRefresh)
        {
            timer = new Timer() { Enabled = false, Interval = 1200 };
            timer.Elapsed += Timer_Elapsed;
            refresh = funcRefresh;
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            refresh(4);
        }

        public void TurnOnOff()
        {
            timer.Enabled = !timer.Enabled;
        }
    }
}
