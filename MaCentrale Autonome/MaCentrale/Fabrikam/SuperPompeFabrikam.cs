﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Fabrikam
{
    public class SuperPompeFabrikam
    {
        public delegate void RefroidirCentrale(int temp);

        private RefroidirCentrale refroidir;
        private Timer timer;
        public bool EstEnMarche { get { return timer.Enabled; } }
        public SuperPompeFabrikam(RefroidirCentrale funcRefresh)
        {
            timer = new Timer() { Enabled = false, Interval = 700 };
            timer.Elapsed += Timer_Elapsed;
            refroidir = funcRefresh;
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            refroidir(6);
        }

        public void MettreEnMarche()
        {
            timer.Enabled = true;
        }
        public void Eteindre()
        {
            timer.Enabled = false;
        }
    }
}
