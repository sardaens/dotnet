﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreCentral
{
    public class Central
    {
        public event StartStop TooHot;
        public event StartStop TooCold;
        public int Temperature { get; private set; }
        public Central()
        {
            Temperature = 20;
        }
        public void Run()
        {
            Temperature += 10;
            if (Temperature >= 80)
                if (TooHot != null)
                    TooHot();
            if (Temperature < 20)
                if (TooCold != null)
                    TooCold();
        }
        public void AddPump(StartStop SwitchOn, StartStop SwitchOff)
        {
            TooHot += SwitchOn;
            TooCold += SwitchOff;
        }
        public void RemovePump(StartStop SwitchOn, StartStop SwitchOff)
        {
            TooHot -= SwitchOn;
            TooCold -= SwitchOff;
            SwitchOff();
        }

        public void Refresh(int temp)
        {
            Temperature -= temp;
        }
    }
}