﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StdAbsDerivated
{
    public class DerivatedClass : AbstractClass
    {
        public override void h()
        {
        }

        [Obsolete]
        public void f()
        {
        }

        public override void j()
        {
            base.j();
        }

        public new void l()
        {
        }
    }
}
