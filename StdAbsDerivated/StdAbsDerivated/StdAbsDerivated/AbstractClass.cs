﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StdAbsDerivated
{
    public abstract class AbstractClass
    {
        public void g()
        {
        }
        public abstract void h();

        public void l()
        {
        }
        public virtual void k()
        {

        }
        public virtual void j()
        {

        }
    }
}
