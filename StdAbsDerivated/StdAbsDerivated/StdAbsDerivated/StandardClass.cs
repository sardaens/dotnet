﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StdAbsDerivated
{
    public class StandardClass
    {
        private int counter;
        private int counter2;

        public StandardClass() : this(5)
        {
            // 5, 4
        }
        public StandardClass(int initValue) : this(initValue, 4)
        {
            // initValue, 4
        }
        public StandardClass(int initValue, int initValue2)
        {
            counter = initValue;
            counter2 = initValue2;
        }

        public int MyProperty { get; set; }
        private int maProp;
        public int MaProp
        { get { return maProp; } set { maProp = value; } }

        public void f()
        {
            counter++;
        }

        public static void s()
        {
        }
    }
}
