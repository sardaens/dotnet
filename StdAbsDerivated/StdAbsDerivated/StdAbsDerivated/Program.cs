﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StdAbsDerivated
{
    class Program
    {
        static void Main(string[] args)
        {
            var c = new StandardClass();
            c.f();
            StandardClass.s();
            AbstractClass a = new DerivatedClass();
            a.g(); // Code de la classe abstraite AbstractClass qui est appelé
            a.h(); // La méthode h étant définie comme abstraite dans la classe de base, 
                   //c'est le code de la classe dérivée qui est appelé
            a.l(); // Code de la classe abstraite AbstractClass qui est appelé, même si il est 
                   // substitué dans la classe dérivée
            a.k(); // Code de la classe abstraite AbstractClass qui est appelé car la classe dérivée
                   // ne surcharge pas le code de la classe abstraite
            a.j(); // C'est le code de la classe dérivée qui est appelé
            var d = (DerivatedClass)a;
            d.f();
            d.l(); // Ici c'est le code substitué qui est appelé, pas celui de la classe de base
        }

    }
}
