﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private Bank ca;

        private void Form1_Load(object sender, EventArgs e)
        {
            ca = new Bank();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Account account = new Account(tbTitulaire.Text,tbMontant.Text);
            //account.Titulaire = textBox1.Text;
            ca.openAccount(account);
            refreshListAccount();
        }

        private void refreshListAccount()
        {
            lbAccount.Items.Clear();
            lvAccount.Items.Clear();
            foreach (var account in ca.getListAccount())
            {
                lbAccount.Items.Add(account);
                lvAccount.Items.Add(account.ToString());
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Account compte = findAccountByTitulaire(tbTitulaire.Text);
            if (compte == null)
            {
                MessageBox.Show("Account not found");
            }
            compte.add(double.Parse(tbMontant.Text));
            refreshListAccount();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Account compte = findAccountByTitulaire(tbTitulaire.Text);
            if (compte == null)
            {
                MessageBox.Show("Account not found");
            }
            compte.withdraw(double.Parse(tbMontant.Text));
            refreshListAccount();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Account compte = findAccountByTitulaire(tbTitulaire.Text); 
            ca.closeAccount(compte);
            refreshListAccount();
        }

        private Account findAccountByTitulaire(String titulaire)
        {
            return (Account)ca.getListAccount().First(account => titulaire== account.Titulaire);
            /*
            foreach (var account in ca.getListAccount())
            {
                if (account.Titulaire.Equals(titulaire))
                {
                    return account;
                }
            }
            return null;
            */
        }
    }
}
