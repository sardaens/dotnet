﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class Account
    {

        private String titulaire;

        private double solde;

        public Account(string titulaire, string solde)
        {
            this.titulaire = titulaire;
            this.solde = double.Parse(solde);
        }

        public string Titulaire
        {
            get
            {
                return titulaire;
            }

            set
            {
                titulaire = value;
            }
        }

        public void add(double money)
        {
            solde += money;
        }

        public void withdraw(double money)
        {
            solde -= money;
        }

        public double getSolde()
        {
            return solde;
        }

        public override string ToString()
        {
            return titulaire + ":" + solde.ToString();
        }
    }
}
