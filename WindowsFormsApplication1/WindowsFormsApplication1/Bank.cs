﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class Bank
    {

        private List<Account> listAccount;

        public Bank()
        {
            listAccount = new List<Account>();
        }

        public void openAccount(Account account)
        {
            if (listAccount.Any(p => p.Titulaire == account.Titulaire))
            {
                throw new AccountAlreadyExistException();
            }
            listAccount.Add(account);
        }

        public void closeAccount(Account account)
        {
            Account result = listAccount.Single(p => p.Titulaire == account.Titulaire);

            if (result == null)
            {
                throw new AccountNotFoundException();
            }
            listAccount.Remove(account);
        }

        public List<Account> getListAccount()
        {
            return listAccount;
        }

    }
}
