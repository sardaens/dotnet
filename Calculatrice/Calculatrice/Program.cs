﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculatrice
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Calcul:");
            String calcul = Console.ReadLine();
            Console.WriteLine("Resultat: {0}", Calcul(calcul));
            Console.ReadLine();
        }

        private static string Calcul(string calcul)
        {
            while (calcul.Contains("("))
            {
                int derniereParentheseOuvrante= calcul.LastIndexOf("(");
                int premiereParentheseFermanteSuivante = calcul.IndexOf(")",derniereParentheseOuvrante);
                int lgEntreParenthese = premiereParentheseFermanteSuivante - derniereParentheseOuvrante;
                string replaceString = calcul.Substring(derniereParentheseOuvrante + 1, lgEntreParenthese-1);
                string resultatCalcul = Calcul(replaceString);
                calcul = calcul.Replace("(" + replaceString + ")", resultatCalcul);

            }
            while(calcul.Contains("*") || calcul.Contains("/"))
            {
                var splitMultiDiv = calcul.Split('*', '/');
                string operandeAvant = splitMultiDiv[0].Split('+', '-').Last();
                string operandeApres = splitMultiDiv[1].Split('+', '-')[0];
                decimal opA = decimal.Parse(operandeAvant);
                decimal opB = decimal.Parse(operandeApres);
                decimal result;
                string op;
                if (calcul.Contains("*"))
                {
                    op = "*";
                    result = opA * opB;
                }
                else
                {
                    op = "/";
                    result = opA / opB;
                }
                calcul = calcul.Replace(operandeAvant + op + operandeApres, result.ToString());
            }
            while (calcul.Contains("+") || calcul.Contains("-"))
            {
                var splitMultiDiv = calcul.Split('+', '-');
                string operandeAvant = splitMultiDiv[0];
                string operandeApres = splitMultiDiv[1];
                decimal opA = decimal.Parse(operandeAvant);
                decimal opB = decimal.Parse(operandeApres);
                
                decimal result;
                string op;
                if (calcul.Contains("+"))
                {
                    op = "+";
                    result = opA + opB;
                }
                else
                {
                    op = "-";
                    result = opA - opB;
                }

                calcul = calcul.Replace(operandeAvant + op + operandeApres, result.ToString());
            }

            return calcul;

        }
    }
}
