﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    /// <summary>
    /// 
    /// </summary>
    class Program
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {

            
            decimal num;
            decimal denom;

            while (true)
            {
                Console.Write("Numérateur ?");
                Saisie(out num, out denom);

                try
                {
                    Console.WriteLine("Résultat: {0}", num / denom);
                }
                catch (DivideByZeroException)
                {
                    Console.Error.WriteLine("Division par zéro impossible !, retry");
                }
                finally
                {
                    Console.WriteLine("-------------------------");
                }

            }


            /*
            int number = int.MaxValue;

            try
            {
                checked
                {
                    number++;
                }
                Console.WriteLine(number);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            */
            /*
             * 
             * 
            Console.WriteLine("Phrase ?");
            String line = Console.ReadLine();


            // for
            for(int k=0;k<line.Length; k++)
            {
                Console.WriteLine(line[k]);
            }
            System.Diagnostics.Debug.Print("debug");

            // foreach
            foreach(char c in line)
            {
                Console.WriteLine(c);
            }


            // while
            int i = 0;
            while(i<line.Length)
            {
                Console.WriteLine(line[i]);
                i++;
            }
            Console.WriteLine(  );
            //LINQ
            line.ToList().ForEach(e => Console.WriteLine(e));
            
            Console.ReadLine();
            */
        }

        private static void Saisie(out decimal num, out decimal denom)
        {
            if (!decimal.TryParse(Console.ReadLine(), out num))
            {
                Console.WriteLine("Numérateur non numérique!");

            }

            Console.Write("Dénominateur ?");
            if (!decimal.TryParse(Console.ReadLine(), out denom))
            {
                Console.WriteLine("Dénominateur non numérique!");
            }
        }
    }
}
