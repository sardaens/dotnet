﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplicationException
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Please, Enter phrase ?");
                String phrase = Console.ReadLine();

                String[] words = phrase.Split(' ');
                int posPoint = phrase.IndexOf(".");

                try
                {
                    checkUserInput(phrase, posPoint, words);

                    Console.WriteLine("Nb de mots: {0}", words.Length);
                    words.Max(p => p.Length);
                    int maxCar = 0;
                    String maxWord = "";
                    foreach (String s in words)
                    {
                        int lg = s.Length;

                        if (s.EndsWith(".")) lg--;
                        if (lg > maxCar)
                        {
                            maxCar = lg;
                            if (s.EndsWith("."))
                                maxWord = s.Substring(0, s.Length - 1);
                            else
                                maxWord = s;

                        }
                    }

                    Console.WriteLine("Mot le + long: {0}", maxWord);

                }
                catch (ApplicationException e)
                {
                    Console.WriteLine(e.Message);
                }
                


                Console.ReadLine();
            }
        }

        private static void checkUserInput(string phrase, int posPoint, String[] words)
        {
            if (phrase.Substring(phrase.Length-1) != ".")
            {
                throw new LastCarIsNotPointException("La phrase ne se termine pas par un point");

            }
            if ((posPoint != -1) && posPoint + 1 < phrase.Length)
            {
                throw new TooManyPointException("La phrase contient plusieurs points");

            }
            else if (phrase != "" && !phrase.StartsWith(phrase[0].ToString().ToUpper()))
            {
                throw new NotStartedByUppercaseException("La phrase ne se commence pas par une majuscule");
            }
            else if (words.Length < 2)
            {
                throw new AtLeastTwoWordsException("La phrase doit contenir au moins deux mots");
            }

        }
    }
}
