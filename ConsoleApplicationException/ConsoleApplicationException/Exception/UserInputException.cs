﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplicationException
{
    class UserInputException : ApplicationException
    {

        public UserInputException(String msg) : base(msg) { }

        public UserInputException(String msg, Exception e) : base(msg,e) { }
    }
}
