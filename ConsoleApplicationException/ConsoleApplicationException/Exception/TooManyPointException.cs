﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplicationException
{
    class TooManyPointException : ApplicationException
    {

        public TooManyPointException(String msg) : base(msg) { }

        public TooManyPointException(String msg, Exception e) : base(msg,e) { }
    }
}
