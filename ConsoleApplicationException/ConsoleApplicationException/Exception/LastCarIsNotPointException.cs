﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplicationException
{
    class LastCarIsNotPointException : ApplicationException
    {

        public LastCarIsNotPointException(String msg) : base(msg) { }

        public LastCarIsNotPointException(String msg, Exception e) : base(msg,e) { }
    }
}
