﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplicationException
{
    class AtLeastTwoWordsException : ApplicationException
    {

        public AtLeastTwoWordsException(String msg) : base(msg) { }

        public AtLeastTwoWordsException(String msg, Exception e) : base(msg,e) { }
    }
}
