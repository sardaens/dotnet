﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplicationException
{
    class NotStartedByUppercaseException : ApplicationException
    {

        public NotStartedByUppercaseException(String msg) : base(msg) { }

        public NotStartedByUppercaseException(String msg, Exception e) : base(msg,e) { }
    }
}
