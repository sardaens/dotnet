﻿using ClassValorisation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleValorisation
{
    class Program
    {
        static void Main(string[] args)
        {

            List<ITicket> listTickets = new List<ITicket>();

            using (StreamReader ficComm = new StreamReader("fic_comm.txt"))
            {
                String line = "";
                while ((line = ficComm.ReadLine()) != null)
                {
                    String[] elts = line.Split(';');

                    TicketComm ticketComm = new ClassValorisation.TicketComm();

                    ticketComm.numeroEmetteur = elts[0];
                    ticketComm.numeroAppel = elts[1];
                    ticketComm.date = DateTime.ParseExact(elts[2], "yyyyMMdd HH:mm:ss", CultureInfo.InvariantCulture);
                    ticketComm.duree = int.Parse(elts[3]);

                    listTickets.Add(ticketComm);
                } 
            }

            using (StreamReader ficSms = new StreamReader("fic_sms.txt"))
            {
                String line = "";
                while ((line = ficSms.ReadLine()) != null)
                {

                    String[] elts = line.Split(';');

                    TicketSMScs ticketSms = new ClassValorisation.TicketSMScs();

                    ticketSms.numeroEmetteur = elts[0];
                    ticketSms.numeroAppel = elts[1];
                    ticketSms.date = DateTime.ParseExact(elts[2], "yyyyMMdd HH:mm:ss", CultureInfo.InvariantCulture);
                    ticketSms.nbCar = int.Parse(elts[3]);
                    listTickets.Add(ticketSms);
                } 

            }
            // write facturation

            using (StreamWriter ficFactu = new StreamWriter("fic_facturation.txt",true))
            {
                listTickets.Sort(new TicketDateComparer());
                String svgdDate = "";
                String svgdEmetteur = "";
                double mttMois = 0;
                foreach (ITicket ticket in listTickets)
                {
                    if (svgdDate != ticket.date.Year.ToString() + ticket.date.Month.ToString("00"))
                    {
                        if (svgdDate != "")
                        {
                            ficFactu.WriteLine(svgdDate + ";" + svgdEmetteur + ";" + mttMois);
                        }
                        svgdDate = ticket.date.Year.ToString() + ticket.date.Month.ToString("00");
                        mttMois = 0;
                        svgdEmetteur = ticket.numeroEmetteur;
                    }
                    mttMois += ticket.Calcul();

                }
                ficFactu.WriteLine(svgdDate + ";" + svgdEmetteur + ";" + mttMois.ToString("0.000"));
            }
        }
    }
}
