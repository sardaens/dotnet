﻿using ClassValorisation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleValorisation
{
    class TicketDateComparer : IComparer<ITicket>
    {
        int IComparer<ITicket>.Compare(ITicket x, ITicket y)
        {
            return x.date.CompareTo(y.date);
        }
    }
}
