﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassValorisation
{
    public class TicketComm : ITicket
    {

        const double COUTS = 0.001d;

        public int duree;

        private DateTime dateTicket;

        private string numeroAppelTicket;

        private string numeroEmetteurTicket;


        public DateTime date
        {
            get
            {
                return dateTicket;
            }

            set
            {
                dateTicket = value;
            }
        }

        public string numeroAppel
        {
            get
            {
                return numeroAppelTicket;
            }

            set
            {
                numeroAppelTicket = value;
            }
        }

        public string numeroEmetteur
        {
            get
            {
                return numeroEmetteurTicket;
            }

            set
            {
                numeroEmetteurTicket = value;
            }
        }


        int dureeAppel
        {
            get
            {
                return duree;
            }

            set
            {
                duree = value;
            }
        }

        double ITicket.Calcul()
        {
            return COUTS * duree;
        }
    }
}
