﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClassValorisation
{
    public interface ITicket
    {
        string numeroEmetteur
        {
            get;
            set;
        }

        string numeroAppel
        {
                    get;
                    set;
        }

        DateTime date 
        {
                    get;
                    set;
        }


        double Calcul();


    }
}
