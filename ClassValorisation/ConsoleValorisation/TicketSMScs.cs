﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassValorisation
{
    public class TicketSMScs : ITicket
    {
        const double COUTS = 0.07d;


        public int nbCar;

        private DateTime dateTicket;

        private string numeroAppelTicket;

        private string numeroEmetteurTicket;


        public DateTime date
        {
            get
            {
                return dateTicket;
            }

            set
            {
                dateTicket = value;
            }
        }

        public string numeroAppel
        {
            get
            {
                return numeroAppelTicket;
            }

            set
            {
                numeroAppelTicket = value;
            }
        }

        public string numeroEmetteur
        {
            get
            {
                return numeroEmetteurTicket;
            }

            set
            {
                numeroEmetteurTicket = value;
            }
        }

        double ITicket.Calcul()
        {
            int res = nbCar / 160;
            return res * COUTS;
        }
    }
}
