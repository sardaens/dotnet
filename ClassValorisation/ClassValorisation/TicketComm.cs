﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassValorisation
{
    public class TicketComm : ITicket
    {
        

        public int duree;

        private DateTime dateTicket;

        private string numeroAppel;

        private string numeroEmetteur;


        DateTime ITicket.date
        {
            get
            {
                return dateTicket;
            }

            set
            {
                dateTicket = value;
            }
        }

        string ITicket.numeroAppel
        {
            get
            {
                return numeroAppel;
            }

            set
            {
                numeroAppel = value;
            }
        }

        string ITicket.numeroEmetteur
        {
            get
            {
                return numeroEmetteur;
            }

            set
            {
                numeroEmetteur = value;
            }
        }


        int dureeAppel
        {
            get
            {
                return duree;
            }

            set
            {
                duree = value;
            }
        }

        double ITicket.Calcul()
        {
            return 0.001 * duree;
        }
    }
}
