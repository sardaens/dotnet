﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassValorisation
{
    public class TicketSMScs : ITicket
    {
        private DateTime dateTicket;

        private string numeroAppel;

        private string numeroEmetteur;

        public int nbCar;

        DateTime ITicket.date
        {
            get
            {
                return dateTicket;
            }

            set
            {
                dateTicket = value;
            }
        }

        string ITicket.numeroAppel
        {
            get
            {
                return numeroAppel;
            }

            set
            {
                numeroAppel = value;
            }
        }

        string ITicket.numeroEmetteur
        {
            get
            {
                return numeroEmetteur;
            }

            set
            {
                numeroEmetteur = value;
            }
        }

        double ITicket.Calcul()
        {
            int res = nbCar / 160;
            return res * 0.07;
        }
    }
}
