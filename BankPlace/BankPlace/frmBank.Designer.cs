﻿namespace BankPlace
{
    partial class frmBank
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOpenAccount = new System.Windows.Forms.Button();
            this.btnCloseAccount = new System.Windows.Forms.Button();
            this.btnListAccount = new System.Windows.Forms.Button();
            this.lblOwner = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtOwner = new System.Windows.Forms.TextBox();
            this.txtAmount = new System.Windows.Forms.NumericUpDown();
            this.lbAccount = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount)).BeginInit();
            this.SuspendLayout();
            // 
            // btnOpenAccount
            // 
            this.btnOpenAccount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOpenAccount.Location = new System.Drawing.Point(12, 71);
            this.btnOpenAccount.Name = "btnOpenAccount";
            this.btnOpenAccount.Size = new System.Drawing.Size(426, 23);
            this.btnOpenAccount.TabIndex = 0;
            this.btnOpenAccount.Text = "Open Account";
            this.btnOpenAccount.UseVisualStyleBackColor = true;
            this.btnOpenAccount.Click += new System.EventHandler(this.btnOpenAccount_Click);
            // 
            // btnCloseAccount
            // 
            this.btnCloseAccount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCloseAccount.Location = new System.Drawing.Point(12, 114);
            this.btnCloseAccount.Name = "btnCloseAccount";
            this.btnCloseAccount.Size = new System.Drawing.Size(426, 23);
            this.btnCloseAccount.TabIndex = 1;
            this.btnCloseAccount.Text = "Close Account";
            this.btnCloseAccount.UseVisualStyleBackColor = true;
            // 
            // btnListAccount
            // 
            this.btnListAccount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnListAccount.Location = new System.Drawing.Point(12, 157);
            this.btnListAccount.Name = "btnListAccount";
            this.btnListAccount.Size = new System.Drawing.Size(426, 23);
            this.btnListAccount.TabIndex = 2;
            this.btnListAccount.Text = "List Account";
            this.btnListAccount.UseVisualStyleBackColor = true;
            this.btnListAccount.Click += new System.EventHandler(this.btnListAccount_Click);
            // 
            // lblOwner
            // 
            this.lblOwner.AutoSize = true;
            this.lblOwner.Location = new System.Drawing.Point(22, 26);
            this.lblOwner.Name = "lblOwner";
            this.lblOwner.Size = new System.Drawing.Size(44, 13);
            this.lblOwner.TabIndex = 3;
            this.lblOwner.Text = "Owner :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(278, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Amount";
            // 
            // txtOwner
            // 
            this.txtOwner.Location = new System.Drawing.Point(73, 26);
            this.txtOwner.Name = "txtOwner";
            this.txtOwner.Size = new System.Drawing.Size(184, 20);
            this.txtOwner.TabIndex = 5;
            // 
            // txtAmount
            // 
            this.txtAmount.DecimalPlaces = 2;
            this.txtAmount.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.txtAmount.Location = new System.Drawing.Point(329, 27);
            this.txtAmount.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(100, 20);
            this.txtAmount.TabIndex = 6;
            this.txtAmount.ThousandsSeparator = true;
            // 
            // lbAccount
            // 
            this.lbAccount.FormattingEnabled = true;
            this.lbAccount.Location = new System.Drawing.Point(12, 201);
            this.lbAccount.Name = "lbAccount";
            this.lbAccount.Size = new System.Drawing.Size(427, 173);
            this.lbAccount.TabIndex = 7;
            // 
            // frmBank
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(451, 388);
            this.Controls.Add(this.lbAccount);
            this.Controls.Add(this.txtAmount);
            this.Controls.Add(this.txtOwner);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblOwner);
            this.Controls.Add(this.btnListAccount);
            this.Controls.Add(this.btnCloseAccount);
            this.Controls.Add(this.btnOpenAccount);
            this.Name = "frmBank";
            this.Text = "Bank";
            ((System.ComponentModel.ISupportInitialize)(this.txtAmount)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOpenAccount;
        private System.Windows.Forms.Button btnCloseAccount;
        private System.Windows.Forms.Button btnListAccount;
        private System.Windows.Forms.Label lblOwner;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtOwner;
        private System.Windows.Forms.NumericUpDown txtAmount;
        private System.Windows.Forms.ListBox lbAccount;
    }
}

