﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BankPlace
{
    public partial class frmBank : Form
    {
        private Bank bank;


        public frmBank()
        {
            InitializeComponent();
            bank = new Bank();
        }

        private void btnOpenAccount_Click(object sender, EventArgs e)
        {
            if (txtOwner.Text ==String.Empty)
            {
                MessageBox.Show("Owner is mandatory.","Owner",MessageBoxButtons.OK);
            }
            try
            {
                bank.OpenAccount(txtOwner.Text, txtAmount.Value);
            }catch(AlreadyCreatedAccount ex)
            {
                MessageBox.Show(ex.Message, "Owner", MessageBoxButtons.OK,MessageBoxIcon.Error);
            }

        }

        private void btnListAccount_Click(object sender, EventArgs e)
        {
            lbAccount.Items.Clear();
            lbAccount.Items.AddRange(bank.ListAllAccounts().ToArray());
        }
    }
}
