﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankPlace
{
    class Account
    {
        public string Owner { get; private set; }
        private decimal amount;

        public Account(string owner, decimal value)
        {
            Owner = owner;
            amount = value;
        }

        public void AddMoney(decimal value)
        {
            amount += value;
        }

        public void WithdrawMoney(decimal value)
        {
            amount -= value;
        }

        public decimal GetAmount()
        {
            return amount;
        }
    }
}
