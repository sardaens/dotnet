﻿using System;
using System.Runtime.Serialization;

namespace BankPlace
{
    [Serializable]
    internal class AlreadyCreatedAccount : Exception
    {
        public AlreadyCreatedAccount()
        {
        }

        public AlreadyCreatedAccount(string message) : base(message)
        {
        }

        public AlreadyCreatedAccount(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected AlreadyCreatedAccount(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}