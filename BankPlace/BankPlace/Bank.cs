﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankPlace
{
    class Bank
    {
        private List<Account> bankAccounts;
        public Bank()
        {
            bankAccounts = new List<Account>();
        }
        public void OpenAccount(string owner, decimal value)
        {
            if (bankAccounts.Any(p => p.Owner == owner))
                throw new AlreadyCreatedAccount();
            bankAccounts.Add(new Account(owner, value));
        }

        public decimal CloseAccount(string owner)
        {
            Account account = bankAccounts.Single(p => p.Owner == owner);
            decimal withdrawSum = account.GetAmount();
            bankAccounts.Remove(account);
            return withdrawSum;
        }

        public void AddMoneyToAccount(string owner, decimal value)
        {
            bankAccounts.Single(p => p.Owner == owner).AddMoney(value);
        }

        public void WithdrawMoneyFromAccount(string owner, decimal value)
        {
            bankAccounts.Single(p => p.Owner == owner).WithdrawMoney(value);
        }

        public List<string> ListAllAccounts()
        {
            return bankAccounts.Select(account => account.Owner).ToList();
        }
    }
}
