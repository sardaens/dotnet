﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplicationADO
{
    class Program
    {
        static void Main(string[] args)
        {

            using (var cn = new SqlConnection(@"Data Source=.\sqlexpress;Initial Catalog=test;Integrated Security=True;Pooling=False"))
            {
                using (var cmd = new SqlCommand("select id, DESCRIPTION from SampleTable;", cn))
                {
                    cn.Open();
                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            Console.WriteLine("{0} {1}", dr[0], dr[1]);
                        }
                    }
                    cn.Close();
                }

                Console.WriteLine("Id ?");
                String id = Console.ReadLine();
                Console.WriteLine("Description ?");
                String desc = Console.ReadLine();



                using (var cmdInsert = new SqlCommand("INSERT INTO SampleTable VALUES (@Value, @Description)", cn))
                {

                    cn.Open();
                    cmdInsert.Parameters.AddWithValue("Value", id);
                    cmdInsert.Parameters.AddWithValue("Description", desc);
                    int i = cmdInsert.ExecuteNonQuery();
                    Console.WriteLine("Nb d'elts ajoutés :{0}", i);
                    cn.Close();
                }

                Console.ReadLine();
            }
        }
    }
}
