﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreCentral
{
    public class Central
    {
        public event StartStop TooHot;
        public event StartStop TooCold;
        public List<Refresh> Pumps;
        private int temperature;
        public int Temperature
        {
            get
            {
                return temperature;
            }
            private set
            {
                temperature = value;
            }
        }
        public Central()
        {
            Temperature = 20;
            Pumps = new List<Refresh>();
        }
        public void Run()
        {
            Temperature += 10;
            if (Temperature >= 80)
                if (TooHot != null)
                    TooHot();
            if (Temperature < 20)
                if (TooCold != null)
                    TooCold();
            foreach (var pump in Pumps)
                pump(ref temperature);
        }
        public void AddPump(StartStop SwitchOn, StartStop SwitchOff, Refresh CoolDown)
        {
            TooHot += SwitchOn;
            TooCold += SwitchOff;
            Pumps.Add(CoolDown);
        }
        public void RemovePump(StartStop SwitchOn, StartStop SwitchOff, Refresh CoolDown)
        {
            TooHot -= SwitchOn;
            TooCold -= SwitchOff;
            Pumps.Remove(CoolDown);
        }

        public void Refresh(int temp)
        {
            temperature -= temp;
        }
    }
}