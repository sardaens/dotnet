﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabrikas
{
    public class ThePump
    {
        public bool IsOn { get; private set; } = false;
        public void TurnOnOff()
        {
            IsOn = !IsOn;
        }
        public void CoolDown(ref int temperature)
        {
            if (IsOn)
                temperature -= 5;
        }
    }
}
