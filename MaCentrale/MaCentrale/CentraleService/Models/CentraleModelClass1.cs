﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CentraleService.Models
{
    public class CentraleModelClass1
    {

        private static CentraleModelClass1 instance;
        private static volatile object lockobj = new object();
        private CoreCentral.Central centrale;

        public CoreCentral.Central {get; set;}

        private CentraleModelClass1()
        { centrale = new CoreCentral.Central(); }

        public static CentraleModelClass1 GetInstance()
        {
            if (instance == null)
            {
                lock (lockobj)
                {
                    if (instance == null)
                    {

                        instance = new CentraleModelClass1();
                    }
                }
            }
            return instance;
        }
            

        
    }
}