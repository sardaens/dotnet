﻿using CoreCentral;
using Fabrikam;
using Fabrikas;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tartampion;

namespace MaCentrale
{
    public partial class frmCentrale : Form
    {
        private Central theOne;
        private ArrayList thePumps;
        private Timer timer;
        #region Ceci est mon constructeur
        public frmCentrale()
        {
            InitializeComponent();
            theOne = new Central();
            thePumps = new ArrayList();
            timer = new Timer() { Enabled = false, Interval = 2000 };
            timer.Tick += Timer_Tick;
        }
        #endregion

        #region Evenements de ma form
        private void Timer_Tick(object sender, EventArgs e)
        {
            theOne.Run();
            lblTemperature.Text = string.Format("{0} °C", theOne.Temperature);
        }

        private void btnRunStop_Click(object sender, EventArgs e)
        {
            timer.Enabled = !timer.Enabled;
            btnRunStop.Text = timer.Enabled ? "Arrêter la centrale" : "Démarrer la centrale";
        }

        private void btnAddFabrikam_Click(object sender, EventArgs e)
        {
            var pump = new SuperPompeFabrikam();
            theOne.AddPump(pump.MettreEnMarche, pump.Eteindre, pump.Refroidir);
            thePumps.Add(pump);
        }

        private void btnAddFabrikas_Click(object sender, EventArgs e)
        {
            var pump = new ThePump();
            theOne.AddPump(pump.TurnOnOff, pump.TurnOnOff, pump.CoolDown);
            thePumps.Add(pump);
        }

        private void btnTartampion_Click(object sender, EventArgs e)
        {
            var pump = new TheWeakestPump();
            theOne.AddPump(pump.TurnOnOff, pump.TurnOnOff, pump.CoolDown);
            thePumps.Add(pump);
        }

        private void btnRemoveFabrikam_Click(object sender, EventArgs e)
        {
            SuperPompeFabrikam pump = (SuperPompeFabrikam)thePumps.ToArray().FirstOrDefault(p => p is SuperPompeFabrikam);
            if (pump != null)
            {
                theOne.RemovePump(pump.MettreEnMarche, pump.Eteindre, pump.Refroidir);
                thePumps.Remove(pump);
            }
        }

        private void btnRemoveFabrikas_Click(object sender, EventArgs e)
        {
            ThePump pump = (ThePump)thePumps.ToArray().FirstOrDefault(p => p is ThePump);
            if (pump != null)
            {
                theOne.RemovePump(pump.TurnOnOff, pump.TurnOnOff, pump.CoolDown);
                thePumps.Remove(pump);
            }
        }

        private void btnRemoveTartampion_Click(object sender, EventArgs e)
        {
            TheWeakestPump pump = (TheWeakestPump)thePumps.ToArray().FirstOrDefault(p => p is TheWeakestPump);
            if (pump != null)
            {
                theOne.RemovePump(pump.TurnOnOff, pump.TurnOnOff, pump.CoolDown);
                thePumps.Remove(pump);
            }
        }
        #endregion
    }
}
