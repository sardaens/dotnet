﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabrikam
{
    public class SuperPompeFabrikam
    {
        public bool EstEnMarche { get; private set; }
        public SuperPompeFabrikam()
        {
            EstEnMarche = false;
        }
        public void MettreEnMarche()
        {
            EstEnMarche = true;
        }
        public void Eteindre()
        {
            EstEnMarche = false;
        }
        public void Refroidir(ref int temperature)
        {
            if (EstEnMarche)
                temperature -= 10;
        }
    }
}
