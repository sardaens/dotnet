﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tartampion
{
    public class TheWeakestPump
    {
        public bool IsOn { get; private set; } = false;
        public void TurnOnOff()
        {
            IsOn = !IsOn;
        }
        public void CoolDown(ref int temperature)
        {
            if (IsOn)
                temperature -= 5;
        }
    }
}
