﻿using Centrale;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tartampion
{
    public class Tartampion : IPompe
    {
        public delegate void CentraleRefroidieEventDelegate(int i);
        public event CentraleRefroidieEventDelegate CentraleRefroidieEvent;

        public bool EstEnMLarche { get; set; }

        public Tartampion()
        {
            EstEnMLarche = false;
        }

        public void MettreEnMarche()
        {
            EstEnMLarche = true;
        }

        public void Eteindre()
        {
            EstEnMLarche = false;
        }

        public void Refroidir(ref int temperature)
        {
            if (EstEnMLarche)
            {
                temperature -= 6;
                if (CentraleRefroidieEvent != null)
                {
                    CentraleRefroidieEvent(6);
                }
            }
        }

        public void Refroidir(ParamTemperature e)
        {
            if (EstEnMLarche)
            {
                e.temperature -= 6;
                if (CentraleRefroidieEvent != null)
                {
                    CentraleRefroidieEvent(6);
                }
            }
        }

    }
}
