﻿using Centrale;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabrikas
{
    public class Fabrikas : IPompe
    {
        public delegate void CentraleRefroidieEventDelegate(int i);
        public event CentraleRefroidieEventDelegate CentraleRefroidieEvent;
        public bool EstEnMLarche { get; set; }

        public Fabrikas()
        {
            EstEnMLarche = false;
        }

        public void MettreEnMarche()
        {
            EstEnMLarche = true;
        }

        public void Eteindre()
        {
            EstEnMLarche = false;
        }

        public void Refroidir(ref int temperature)
        {
            if (EstEnMLarche)
            {
                temperature -= 5;
                if (CentraleRefroidieEvent !=null)
                {
                    CentraleRefroidieEvent(5);
                }
            }
        }


        public void Refroidir(ParamTemperature e)
        {
            if (EstEnMLarche)
            {
                e.temperature -= 5;
                if (CentraleRefroidieEvent != null)
                {
                    CentraleRefroidieEvent(5);
                }
            }
        }
    }

    
}
