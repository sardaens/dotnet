﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Centrale.CoreCentrale;


namespace Centrale
{
    
    public class Centrale
    {

        public delegate void UpdateTempratureDelegate(ParamTemperature e);

        #region event

        public event StartStop TooHotEvent;
        public event StartStop TooColdEvent;
        public event UpdateTempratureDelegate UpdateTempartureEvent;

        // public List<Refresh> pompes;
        #endregion


        private int temperature;

        public int Temperature {
            get
            {
                return temperature;
            }
            set { temperature = value; }
        }

        public Centrale()
        {
            Temperature = 20;

            //pompes = new List<Refresh>();
        }

        public void Run()
        {
            Temperature += 10;

            if (Temperature >= 80)
            {
                if (TooHotEvent !=null)
                {
                    TooHotEvent();
                }
            }
            else
            {
                if (Temperature < 20)
                {
                    if (TooColdEvent != null)
                    {
                        TooColdEvent();
                    }
                }

            }
            if (UpdateTempartureEvent != null)
            {
                ParamTemperature p = new ParamTemperature();
                p.temperature = temperature;
                UpdateTempartureEvent(p);
                temperature = p.temperature;
            }
            /*
            foreach(var pump in pompes)
            {
                pump(ref temperature);
            }
            */

        }

        public void AddPompe(StartStop SwitchOn, StartStop SwitchOff, UpdateTempratureDelegate CoolDown)
        {
            TooHotEvent += SwitchOn;
            TooColdEvent += SwitchOff;
            UpdateTempartureEvent += CoolDown;
            //pompes.Add(CoolDown);
        }

        public void RetrievePompe(StartStop SwitchOn, StartStop SwitchOff, UpdateTempratureDelegate CoolDown)
        {
            TooHotEvent -= SwitchOn;
            TooColdEvent -= SwitchOff;
            UpdateTempartureEvent -= CoolDown;
            //pompes.Remove(CoolDown);
        }

        /*
        public void AddPompe(StartStop SwitchOn, StartStop SwitchOff, Refresh CoolDown)
        {
            TooHotEvent += SwitchOn;
            TooColdEvent += SwitchOff;
            pompes.Add(CoolDown);
        }

        public void RetrievePompe(StartStop SwitchOn, StartStop SwitchOff, Refresh CoolDown)
        {
            TooHotEvent -= SwitchOn;
            TooColdEvent -= SwitchOff;
            pompes.Remove(CoolDown);
        }
        */

        public void Refresh(int temp)
        {
            temperature -= temp;
        }
    }


}
