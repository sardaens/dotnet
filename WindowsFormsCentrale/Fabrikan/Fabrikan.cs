﻿using Centrale;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fabrikan
{
    public class Fabrikan : IPompe
    {

        public delegate void CentraleRefroidieEventDelegate(int i);
        public event CentraleRefroidieEventDelegate CentraleRefroidieEvent;

        public bool EstEnMLarche { get; set; }

        public Fabrikan()
        {
            EstEnMLarche = false;
        }

        public void MettreEnMarche()
        {
            EstEnMLarche = true;
        }

        public void Eteindre()
        {
            EstEnMLarche = false;
        }

        public void Refroidir(ref int temperature)
        {
            if (EstEnMLarche)
            {
                temperature -= 10;
                if (CentraleRefroidieEvent !=null)
                {
                    CentraleRefroidieEvent(10);
                }
            }
        }

        public void Refroidir(ParamTemperature e)
        {
            if (EstEnMLarche)
            {
                e.temperature -= 10;
                if (CentraleRefroidieEvent != null)
                {
                    CentraleRefroidieEvent(10);
                }
            }
        }
    }
}
