﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Fabrikas;
using Centrale;
using System.Collections;

namespace WindowsFormsCentrale
{
    public partial class Form1 : Form
    {

        Centrale.Centrale centrale;

        List<Fabrikas.Fabrikas> listpompesFabrikas = new List<Fabrikas.Fabrikas>();
        List<Fabrikan.Fabrikan> listpompesFabrikan = new List<Fabrikan.Fabrikan>();
        List<Tartampion.Tartampion> listpompestarTempion = new List<Tartampion.Tartampion>();


        public Form1()
        {
            InitializeComponent();
            centrale = new Centrale.Centrale();
        }

        private void btnChauffe_Click(object sender, EventArgs e)
        {
            centrale.Run();
            updateLabelTemperature();

        }

        private void updateLabelTemperature()
        {
            lblTemperature.Text = centrale.Temperature.ToString();
            if (centrale.Temperature >= 80)
                lblTemperature.ForeColor = Color.Red;
            else
                lblTemperature.ForeColor = Color.Green;
        }

        private void btnAddPompe_Click(object sender, EventArgs e)
        {
            // add pompe
            Fabrikas.Fabrikas pompe = new Fabrikas.Fabrikas();
            /*
            centrale.AddPompe(new Centrale.CoreCentrale.StartStop(pompe.MettreEnMarche), 
                new Centrale.CoreCentrale.StartStop(pompe.Eteindre), 
                new Centrale.CoreCentrale.Refresh(pompe.Refroidir));
                */

            centrale.AddPompe(new Centrale.CoreCentrale.StartStop(pompe.MettreEnMarche),
                new Centrale.CoreCentrale.StartStop(pompe.Eteindre),
                new Centrale.Centrale.UpdateTempratureDelegate(pompe.Refroidir));


            listpompesFabrikas.Add(pompe);

            pompe.CentraleRefroidieEvent += Pompe_CentraleRefroidieEvent;

            lblNbFabrikas.Text = listpompesFabrikas.Count.ToString();
        }

        private void Pompe_CentraleRefroidieEvent(int i)
        {
            lbMessage.Items.Add("Pompe refroidie de:" + i + "°");
        }

        private void btnRetirerPompe_Click(object sender, EventArgs e)
        {
            Fabrikas.Fabrikas pompe = (Fabrikas.Fabrikas)listpompesFabrikas.First();

            centrale.RetrievePompe(pompe.MettreEnMarche,pompe.Eteindre,pompe.Refroidir);
            listpompesFabrikas.Remove(pompe);

            lblNbFabrikas.Text = listpompesFabrikas.Count.ToString();
        }

        private void btnAddFabrikan_Click(object sender, EventArgs e)
        {
            // add pompe
            Fabrikan.Fabrikan pompe = new Fabrikan.Fabrikan();

            /*
            centrale.AddPompe(new Centrale.CoreCentrale.StartStop(pompe.MettreEnMarche),
                new Centrale.CoreCentrale.StartStop(pompe.Eteindre),
                new Centrale.CoreCentrale.Refresh(pompe.Refroidir));
                */
            centrale.AddPompe(new Centrale.CoreCentrale.StartStop(pompe.MettreEnMarche),
           new Centrale.CoreCentrale.StartStop(pompe.Eteindre),
           new Centrale.Centrale.UpdateTempratureDelegate(pompe.Refroidir));

            listpompesFabrikan.Add(pompe);

            pompe.CentraleRefroidieEvent += Pompe_CentraleRefroidieEvent;

            lblNbFabrikan.Text = listpompesFabrikan.Count.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Fabrikan.Fabrikan pompe = (Fabrikan.Fabrikan)listpompesFabrikan.First();

            centrale.RetrievePompe(pompe.MettreEnMarche, pompe.Eteindre, pompe.Refroidir);
            listpompesFabrikan.Remove(pompe);

            lblNbFabrikan.Text = listpompesFabrikan.Count.ToString();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            // add pompe
            Tartampion.Tartampion pompe = new Tartampion.Tartampion();

            /*
            centrale.AddPompe(new Centrale.CoreCentrale.StartStop(pompe.MettreEnMarche),
                new Centrale.CoreCentrale.StartStop(pompe.Eteindre),
                new Centrale.CoreCentrale.Refresh(pompe.Refroidir));
                */

            centrale.AddPompe(new Centrale.CoreCentrale.StartStop(pompe.MettreEnMarche),
               new Centrale.CoreCentrale.StartStop(pompe.Eteindre),
               new Centrale.Centrale.UpdateTempratureDelegate(pompe.Refroidir));

            listpompestarTempion.Add(pompe);

            pompe.CentraleRefroidieEvent += Pompe_CentraleRefroidieEvent;

            lblNbTartempion.Text = listpompestarTempion.Count.ToString();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Tartampion.Tartampion pompe = (Tartampion.Tartampion)listpompestarTempion.First();

            centrale.RetrievePompe(pompe.MettreEnMarche, pompe.Eteindre, pompe.Refroidir);
            listpompestarTempion.Remove(pompe);

            lblNbTartempion.Text = listpompestarTempion.Count.ToString();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            centrale.Run();
            updateLabelTemperature();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            lbMessage.Items.Clear();
        }
    }
}
