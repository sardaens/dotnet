﻿namespace WindowsFormsCentrale
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblTemperature = new System.Windows.Forms.Label();
            this.btnChauffe = new System.Windows.Forms.Button();
            this.lbMessage = new System.Windows.Forms.ListBox();
            this.btnAddPompe = new System.Windows.Forms.Button();
            this.btnRetirerPompe = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnAddFabrikan = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.lblNbFabrikas = new System.Windows.Forms.Label();
            this.lblNbFabrikan = new System.Windows.Forms.Label();
            this.lblNbTartempion = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblTemperature
            // 
            this.lblTemperature.AutoSize = true;
            this.lblTemperature.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTemperature.Location = new System.Drawing.Point(67, 78);
            this.lblTemperature.Name = "lblTemperature";
            this.lblTemperature.Size = new System.Drawing.Size(37, 25);
            this.lblTemperature.TabIndex = 0;
            this.lblTemperature.Text = "x °";
            // 
            // btnChauffe
            // 
            this.btnChauffe.Location = new System.Drawing.Point(58, 134);
            this.btnChauffe.Name = "btnChauffe";
            this.btnChauffe.Size = new System.Drawing.Size(86, 23);
            this.btnChauffe.TabIndex = 1;
            this.btnChauffe.Text = "Chauffe +10°";
            this.btnChauffe.UseVisualStyleBackColor = true;
            this.btnChauffe.Click += new System.EventHandler(this.btnChauffe_Click);
            // 
            // lbMessage
            // 
            this.lbMessage.FormattingEnabled = true;
            this.lbMessage.Location = new System.Drawing.Point(58, 309);
            this.lbMessage.Name = "lbMessage";
            this.lbMessage.Size = new System.Drawing.Size(521, 173);
            this.lbMessage.TabIndex = 2;
            // 
            // btnAddPompe
            // 
            this.btnAddPompe.Location = new System.Drawing.Point(318, 34);
            this.btnAddPompe.Name = "btnAddPompe";
            this.btnAddPompe.Size = new System.Drawing.Size(261, 23);
            this.btnAddPompe.TabIndex = 3;
            this.btnAddPompe.Text = "+ Fabrikas";
            this.btnAddPompe.UseVisualStyleBackColor = true;
            this.btnAddPompe.Click += new System.EventHandler(this.btnAddPompe_Click);
            // 
            // btnRetirerPompe
            // 
            this.btnRetirerPompe.Location = new System.Drawing.Point(318, 63);
            this.btnRetirerPompe.Name = "btnRetirerPompe";
            this.btnRetirerPompe.Size = new System.Drawing.Size(261, 23);
            this.btnRetirerPompe.TabIndex = 4;
            this.btnRetirerPompe.Text = "- Fabrikas";
            this.btnRetirerPompe.UseVisualStyleBackColor = true;
            this.btnRetirerPompe.Click += new System.EventHandler(this.btnRetirerPompe_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(318, 134);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(261, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "- Fabrikan";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnAddFabrikan
            // 
            this.btnAddFabrikan.Location = new System.Drawing.Point(318, 105);
            this.btnAddFabrikan.Name = "btnAddFabrikan";
            this.btnAddFabrikan.Size = new System.Drawing.Size(261, 23);
            this.btnAddFabrikan.TabIndex = 5;
            this.btnAddFabrikan.Text = "+ Fabrikan";
            this.btnAddFabrikan.UseVisualStyleBackColor = true;
            this.btnAddFabrikan.Click += new System.EventHandler(this.btnAddFabrikan_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(318, 207);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(261, 23);
            this.button3.TabIndex = 8;
            this.button3.Text = "- Tartempion";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(318, 178);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(261, 23);
            this.button4.TabIndex = 7;
            this.button4.Text = "+ Tartempion";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // lblNbFabrikas
            // 
            this.lblNbFabrikas.AutoSize = true;
            this.lblNbFabrikas.Location = new System.Drawing.Point(286, 54);
            this.lblNbFabrikas.Name = "lblNbFabrikas";
            this.lblNbFabrikas.Size = new System.Drawing.Size(13, 13);
            this.lblNbFabrikas.TabIndex = 9;
            this.lblNbFabrikas.Text = "0";
            // 
            // lblNbFabrikan
            // 
            this.lblNbFabrikan.AutoSize = true;
            this.lblNbFabrikan.Location = new System.Drawing.Point(286, 125);
            this.lblNbFabrikan.Name = "lblNbFabrikan";
            this.lblNbFabrikan.Size = new System.Drawing.Size(13, 13);
            this.lblNbFabrikan.TabIndex = 10;
            this.lblNbFabrikan.Text = "0";
            // 
            // lblNbTartempion
            // 
            this.lblNbTartempion.AutoSize = true;
            this.lblNbTartempion.Location = new System.Drawing.Point(286, 197);
            this.lblNbTartempion.Name = "lblNbTartempion";
            this.lblNbTartempion.Size = new System.Drawing.Size(13, 13);
            this.lblNbTartempion.TabIndex = 11;
            this.lblNbTartempion.Text = "0";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 2000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(268, 504);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 12;
            this.button2.Text = "Clear";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(637, 545);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.lblNbTartempion);
            this.Controls.Add(this.lblNbFabrikan);
            this.Controls.Add(this.lblNbFabrikas);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnAddFabrikan);
            this.Controls.Add(this.btnRetirerPompe);
            this.Controls.Add(this.btnAddPompe);
            this.Controls.Add(this.lbMessage);
            this.Controls.Add(this.btnChauffe);
            this.Controls.Add(this.lblTemperature);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTemperature;
        private System.Windows.Forms.Button btnChauffe;
        private System.Windows.Forms.ListBox lbMessage;
        private System.Windows.Forms.Button btnAddPompe;
        private System.Windows.Forms.Button btnRetirerPompe;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnAddFabrikan;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label lblNbFabrikas;
        private System.Windows.Forms.Label lblNbFabrikan;
        private System.Windows.Forms.Label lblNbTartempion;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button button2;
    }
}

