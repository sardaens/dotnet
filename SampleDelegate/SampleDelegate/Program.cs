﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleDelegate
{
    class Program
    {

        public delegate void MaMethodeDelegate();
        static event MaMethodeDelegate RunAllEvent;

        static void Main(string[] args)
        {

            List<MaMethodeDelegate> mesFonctions = new List<MaMethodeDelegate>();
           
            // enregistre les abonnées
            RunAllEvent += new MaMethodeDelegate(f);
            RunAllEvent += new MaMethodeDelegate(g);
            //mesFonctions.Add(new MaMethode(f));
            //mesFonctions.Add(new MaMethode(g));

            if (RunAllEvent != null)
            {
                RunAllEvent();
            }
            /*
            foreach (MaMethode m in mesFonctions)
            {
                m();
            }
            */
            /*
            maFonction = 
            maFonction();

            maFonction = new MaMethode(g);
0            maFonction();
            */
            Console.ReadLine();
        }


        // methode des abonnés
        static void f()
        {
            Console.WriteLine("je suis la fonction f");

        }

        static void g()
        {
            Console.WriteLine("je suis la fonction g");

        }

        static void h()
        {
            Console.WriteLine("je suis la fonction h");

        }


    }
}
